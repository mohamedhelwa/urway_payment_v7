
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:urwaypayment/Model/TrxnRespModel.dart';
import 'package:urwaypayment/ResponseConfig.dart';
import 'package:flutter/services.dart';


class TransactPage extends StatefulWidget {

  final String inURL;
  TransactPage({Key key,this.inURL}):super(key:key);

  @override
  _TransactPageState createState() => _TransactPageState();
}

class _TransactPageState extends State<TransactPage> {
  InAppWebViewController webView;
  String url = "";
  double progress = 0;
  String payId;
  String responseHash;
  String ResponseCode;
  String transId;
  String amount;
  String cardToken;
  String maskedCardNo;
  String result;
  ProgressDialog pr;

  Future<bool> _onBackPressed() async {
    // Your back press code here...
    ResponseConfig.startTrxn = false;
    Navigator.pop(context);
  }
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
//  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.blueGrey, //or set color with: Color(0xFF0000FF)
    ));
    pr.style(
        message: 'Please wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),

        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    return new WillPopScope(
        onWillPop: _onBackPressed,
        child:
        SafeArea(
        child:Container(
      margin: const EdgeInsets.all(1.0),
      child: InAppWebView(
        initialUrl: widget.inURL,
        initialHeaders: {},
//                    initialOptions: InAppWebViewWidgetOptions(
//                        inAppWebViewOptions: InAppWebViewOptions(
//                          debuggingEnabled: true,
//                        )
//                    ),
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
          pr.show();
        },

        onLoadStart: (InAppWebViewController controller, String url) {
          setState(()
          {
//            pr.show();
              this.url = url;
          });
        },

        onLoadStop: (InAppWebViewController controller, String url) async {
          print(url);
          pr.hide();
          if (url.contains("&Result")) {
//            RegExp regExp = new RegExp("Result=(.*)&Track");
//            token = regExp.firstMatch(url).group(1);
            List<String> arr = url.split('?');
            print(arr[1]);
            String lastData=splitResponse(arr[1]);
            print('Transact $lastData');
            Navigator.pop(context, '$lastData');

//
//            showResDialog( context);
//                            Navigator.of(context, rootNavigator: true)
//                                .push(MaterialPageRoute(
//                                builder: (context) => new HomePage()));

          }
        },
        onProgressChanged: (InAppWebViewController controller, int progress) {
          setState(() {
            this.progress = progress / 100;
          });
        },
      ),
        ),
    ),

    );
  }

  String splitResponse(String resultData)
  {
    List<String> resultParameters=resultData.split("&");
    for (String parameter in resultParameters) {
      List parts = parameter.split("=");
      String name = parts[0];
      print("parameters[] name=" + name);
      if (name=="PaymentId") {
        print("name pay id " + name);
        payId = parts[1].toString();
        print("name pay id " + parts[1]);
      }
      if (name=="responseHash") {
        print("name response hash " + name);
        responseHash = parts[1].toString();
        print('name response hash $parts[1]');
      }
      if (name=="ResponseCode") {
        print("name pay id " + name);
        ResponseCode = parts[1].toString();
        print("name pay id " + parts[1]);
      }
      if (name == "TranId") {
        print("name pay id " + name);
        transId = parts[1].toString();
        print("name pay id " + parts[1]);
      }
      if (name=="amount") {
        print("name pay id " + name);
        amount = parts[1].toString();
        print("name pay id " + parts[1]);
      }

      if (name=="cardToken") {
        print("name pay id " + name);
        cardToken = parts[1].toString();
        print("name pay id " + parts[1]);
      }
      if (name=="Result") {
        print("Result " + name);
        result = parts[1].toString();
        print("name pay id " + parts[1]);
      }

      if (name=="maskedCardPan")
      {
        print("name pay id " + name);
        maskedCardNo = parts[1].toString();
        print("name pay id " + parts[1]);
      }
    }
    TrxnRespModel trxnRespModel=new TrxnRespModel(TranId: payId,ResponseCode: ResponseCode,amount: amount,result:result ,payId: payId,cardToken: cardToken);
    var resp= json.encode(trxnRespModel.toMap());
    ResponseConfig.startTrxn = false;
    return resp;

  }

}
