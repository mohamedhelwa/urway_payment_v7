library urwaypayment;

import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:convert/convert.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/services.dart';
import 'package:get_ip/get_ip.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:urwaypayment/Constantvals.dart';
import 'package:urwaypayment/Model/PayRefundReq.dart';
import 'package:urwaypayment/Model/PayTokenizeReq.dart';
import 'package:urwaypayment/Model/PaymentReq.dart';
import 'package:urwaypayment/Model/PaymentResp.dart';
import 'package:urwaypayment/ResponseConfig.dart';


import 'Model/Post.dart';
import 'Model/TrxnRespModel.dart';
import 'TransactPage.dart';
import 'package:crypto/crypto.dart';
//import 'package:convert/convert.dart';
//import 'dart:convert';

/// A Calculator.
class Payment {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
  ProgressDialog pr;
 /* Future<PaymentResp> fetchPosts(String url) async {


    PaymentReq newPost = new PaymentReq(userId: myString);
    var body = json.encode(Post.toMap());
    print(body);
    Map<String, String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json'
    };



    var response = await http.post(url,headers: headers, body: body);
    print("response"+response.body);
    return compute(parsePosts, response.body);
  }

  void _performTrxn(String amount,BuildContext context) {
    String username ="10";
    String Currency = "SAR";
//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => HelpDesk()),
//    );

    Post newPost = new Post(
        strUsername: username, strPassword:password*//* title: titleControler.text, body: bodyControler.text*//*);

    fetchPosts(APIUrlConstant.CREATE_POST_LoginURL,
        body1: newPost.toMap());
//      print(p.trxnStatus);

    print('login attempt: $username with $password');
//  return username+":"+password;
  }*/

  static Future<String> makepaymentService ({
    @required BuildContext context,String country,String action,String currency,String amt,String customerEmail,String merchantIp,String trackid,String udf1,String udf2,String udf3,String udf4,String udf5,String address,String city,String zipCode,String state,String cardToken,String tokenizationType,String tokenOperation
  }) async {
    assert(context != null, "context is null!!");
    if (ResponseConfig.startTrxn != Constantvals.appinitiateTrxn) {
      ResponseConfig.startTrxn = true;

      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          var order = await _read(
              context,
              country,
              action,
              currency,
              amt,
              customerEmail,
              merchantIp,
              trackid,
              udf1,
              udf2,
              udf3,
              udf4,
              udf5,
              address,
              city,
              zipCode,
              state,
              cardToken,
              tokenizationType,tokenOperation);
          return order;
        }
      } on SocketException catch (e)
    {
    ResponseConfig.startTrxn = false;
        return "Please check internet connection";

      }

    }
    else
    {
      return "Transaction already initiated";
    }


//      showDialog(
//        context: context,
//        builder: (context) {
//          return AlertDialog(
//            shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(15)),
//            ),
//            content: Column(
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                willDisplayWidget,
//                RaisedButton(
//                  color: Colors.white30,
//                  child: Text('Close' ),
//                  onPressed: () {
//                    Navigator.of(context).pop();
//                  },
//                )
//              ],
//            ),
//            elevation: 10,
//          );
//        });
  }

  static Future<String> _read(BuildContext context,String country,String action,String currency,String amt,String customerEmail,String merchantIp,String trackid,String udf1,String udf2,String udf3,String udf4,String udf5,String address,String city,String zipCode,String state,String cardToken,String tokenizationType,String tokenOperation)  async {
    String text;
//    InAppWebViewController webView;
    String url = "";
    double progress = 0;
    String pipeSeperatedString;
    var body;
    ResponseConfig resp = ResponseConfig();
    var ipAdd;
    PaymentReq payment;
    ProgressDialog pr = new ProgressDialog(context);
    String compURL;
//    WifiIpInfo _result;
    pr.style(
        message: 'Please wait1...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );


    text =
    await DefaultAssetBundle.of(context).loadString('assets/appconfig.json');
    final jsonResponse = json.decode(text);
    print(jsonResponse);
    var t_id = jsonResponse["terminalId"] as String;
    var t_pass = jsonResponse["terminalPass"] as String;
    var merc = jsonResponse["merchantKey"] as String;
    var req_url = jsonResponse["requestUrl"] as String;
    Constantvals.termId = t_id;
    Constantvals.termpass = t_pass;
    Constantvals.merchantkey = merc;
    Constantvals.requrl = req_url;

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      print("Connected to Mobile Network");
      ipAdd = await GetIp.ipAddress;
      if(merchantIp == '' || merchantIp == null)
        {
          merchantIp=await GetIp.ipAddress;
          print('merchantIP  mobile $merchantIp');
        }
    } else if (connectivityResult == ConnectivityResult.wifi) {
      print("Connected to WiFi");
      var wifiIp = await Connectivity().getWifiIP();
      ipAdd=wifiIp;
      print('$wifiIp');
      if(merchantIp == '' || merchantIp == null)
      {
        merchantIp= await Connectivity().getWifiIP();
        print('merchantIP  wifi $merchantIp');
      }
    }
    else {
      print("Unable to connect. Please Check Internet Connection");
    }

    if (isValidationSucess(
        context,
        amt,
        customerEmail,
        action,
        country,
        currency,
        trackid,
        tokenOperation,
        cardToken)) {
      //check validation
//      pr.show();
      pipeSeperatedString =
          trackid + "|" + Constantvals.termId + "|" + Constantvals.termpass +
              "|" +
              Constantvals.merchantkey + "|" + amt + "|" + currency;
      print('$pipeSeperatedString');
      var bytes = utf8.encode(pipeSeperatedString);
      Digest sha256Result = sha256.convert(bytes);
      final digestHex = hex.encode(sha256Result.bytes);

      //String hashVal=sha256Result.toString();
      print('HashVal $digestHex');
      if (action == '1' || action=='4') {
        payment = new PaymentReq(terminalId: t_id,
            password: t_pass,
            action: action,
            currency: currency,
            customerEmail: customerEmail,
            country: country,
            amount: amt,
            customerIp: ipAdd,
            merchantIp: merchantIp,
            trackid: trackid,
            udf1: udf1,
            udf2: udf2,
            udf3: udf3,
            udf4: udf4,
            udf5: udf5,
            udf7: "ANDROID",
            address: address,
            city: city,
            zipCode: zipCode,
            state: state,
            cardToken: cardToken,
            tokenizationType: tokenizationType,
            requestHash: digestHex);
        body = json.encode(payment.toMap());
        print('action code 1');
        print('mercha $merchantIp');

      }
      else if (action == '12')
      {
       PayTokenizeReq payTokenize = new PayTokenizeReq(terminalId: t_id,
            password: t_pass,
            action: action,
            currency: currency,
            customerEmail: customerEmail,
            country: country,
            amount: amt,
            customerIp: ipAdd,
            merchantIp: merchantIp,
            trackid: trackid,
            udf1: udf1,
            udf2: udf2,
            udf3: udf3,
            udf4: udf4,
            udf5: udf5,
            udf7: "ANDROID",
            cardToken: cardToken,
            requestHash: digestHex,
            tokenOperation: tokenOperation);
        print('action code 12');
        body = json.encode(payTokenize.toMap());
      }
      else if (action == '14') {
        PayRefundReq payRefundReq = new PayRefundReq(terminalId: t_id,
            password: t_pass,
            action: action,
            currency: currency,
            customerEmail: customerEmail,
            country: country,
            amount: amt,
            customerIp: ipAdd,
            merchantIp: merchantIp,
            trackid: trackid,
            udf1: udf1,
            udf2: udf2,
            udf3: udf3,
            udf4: udf4,
            udf5: udf5,
            udf7: "ANDROID",
            cardToken: cardToken,
            requestHash: digestHex);
        print('action code 14');
        body = json.encode(payRefundReq.toMap());
      }
      print('Payment Request $body');
      try {

//  var body = '{"transid":"2011919515049822299","amount":"1.00","address":"mahape","customerIp":"10.10.10.227","city":"navi mumbai","trackid":"12121212","terminalId":"recterm","action":"14","password":"password","merchantIp":"9.10.10.102","requestHash":"746c98bf1f13dbb708deb6d2d22b798860f543e9db292bfb475a4b1045ba5e87","country":"IN","currency":"SAR","customerEmail":"swapnil.kapse@concertosoft.com","zipCode":"410209","udf3":"","udf1":"","udf2":"","udf4":"","udf5":"","cardToken":"9114020300486869","tokenizationType":"0","cardholdername":"Akshay Jadhav","instrumentType":"DEFAULT"}';
        print('API nRequest $body');
        print(body);
        Map<String, String> headers = {
          'Content-type': 'application/json',
          'Accept': 'application/json'
        };

//5123450309390008
        var response = await http.post(
            Constantvals.requrl, headers: headers, body: body);
        print("sucessful api calling");
        print(response.statusCode);
        if (response.statusCode == 200) {
          print(response.body.toString());
          var data = json.decode(response.body);
          var payId = data["payid"] as String;
          var tar_url = data["targetUrl"] as String;
          var resp_code = data["responseCode"] as String;

          pr.hide();
          if (tar_url != null && !tar_url.isEmpty) {
            print(tar_url);

            //Todo check for ? if already there then don put
            if(tar_url.endsWith('?'))
              {
                compURL = tar_url + "paymentid=" + payId;
              }

            else
              {
                compURL = tar_url + "?paymentid=" + payId;
              }
//            = tar_url + "?paymentid=" + payId;
            print(compURL);
            String result = await Navigator.of(context).push(
                MaterialPageRoute<String>(builder: (BuildContext context) {
                  return new TransactPage(inURL: compURL);
                }));
            print('Navigator Otput $result');
//            if(pr.isShowing())
//            {
//              pr.hide();
//            }
            if(result == null )
            {
              result='';
//              if(pr.isShowing() && pr == null)
//              {
//                pr.hide();
//              }
            }
            ResponseConfig.startTrxn = false;
            return result;
          }
          else if(tar_url == null && resp_code== '000' )
            {

              var payId = data["tranid"] as String;
              var ResponseCode = data["ResponseCode"] as String;
              var amount = data["amount"] as String;
              var result = data["result"] as String;
              var transId = data["PaymentId"] as String;
              var cardToken = data["cardToken"] as String;

              TrxnRespModel trxnRespModel=new TrxnRespModel(TranId: transId,ResponseCode: ResponseCode,amount: amount,result:result ,payId: payId,cardToken: cardToken);
              var resp= json.encode(trxnRespModel.toMap());
              ResponseConfig.startTrxn = false;
              return resp;

            }
          else {
//          Boolean bool = str.toLowerCase().contains(test.toLowerCase());
            var ErrorMsg;
            var apirespCode = data["responseCode"] as String;
            if (apirespCode == null) {
              apirespCode = data["responsecode"] as String;
              ErrorMsg = resp.respCode['$apirespCode'];
            }
            else {
              ErrorMsg = resp.respCode['$apirespCode'];
            }

            var apiresult = data["result"] as String;

            print('ResponseCode $apirespCode : $ErrorMsg');
            showalertDailog(context, '$apiresult', '$ErrorMsg');

            /*showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                  ),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('Data $ErrorMsg'),
                      RaisedButton(
                        color: Colors.white30,
                        child: Text('Close' ),
                        onPressed: ()
                        {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                  elevation: 10,
                );
              });*/
            //handle with dailog and  responseconfig=flase
//          return respCode;
          }
        }
        else {

//          pr.hide();
          showalertDailog(context, 'Error', 'Invalid Request');

          /*  showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RaisedButton(
                  color: Colors.white30,
                  child: Text('Close' ),
                  onPressed: ()
                  {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
            elevation: 10,
          );
        });*/
        }
      }
        on Exception {
          ResponseConfig.startTrxn = false;
          print('In Exception of urway payment ');
//          pr.hide();
        }
      }
    else {
//      pr.hide();
      ResponseConfig.startTrxn = false;
    }

  }
//Todo  failed api response   responseconfig=flase |
    //Capture Proper Response and work

   static bool isValidationSucess(BuildContext context,String amount,String email,String Action,String CountryCode,String Currency,String track,String cardOperation,String cardToken)
  {
    bool d = false;

//    String chEmai= validateEmail(email);
//    print('$chEmai');
    final bool isValidEmail = EmailValidator.validate(email);
    bool isValidE=isValidEmailchk(email);
    print('$cardOperation : $isValidE');
    if (amount.isEmpty) {
      showalertDailog(context,'Error','Amount should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if (email.isEmpty ) {
      showalertDailog(context,'Error','Email should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if (Action.isEmpty || Action.length==0) {
      showalertDailog(context,'Error','Action Code should not be empty');
      ResponseConfig.startTrxn = false;
    }

    else if (Currency.isEmpty || Currency.length==0) {
      showalertDailog(context,'Error','Currency should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if (CountryCode.isEmpty || CountryCode.length==0) {
      showalertDailog(context,'Error','Country Code should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if (track.isEmpty || track.length==0)
    {
      showalertDailog(context,'Error','Track ID should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if(Currency.length>3)
    {
      showalertDailog(context,'Error','Currency should be proper');
      ResponseConfig.startTrxn = false;
    }

    else if(Action.length>3)
    {
      showalertDailog(context,'Error','Action Code should be proper ');
      ResponseConfig.startTrxn = false;
    }
    else if(CountryCode.length>2)
    {
      showalertDailog(context,'Error','CountryCode should be proper');
      ResponseConfig.startTrxn = false;
    }
        else if(email.isEmpty)
        {
          showalertDailog(context,'Error','Email should not be empt');
          ResponseConfig.startTrxn = false;
        }
    else if(!email.isEmpty && (isValidEmail==false))
    {
      showalertDailog(context,'Error','Email should be proper');
      ResponseConfig.startTrxn = false;
    }
    else if(((Action=='12')&&(cardOperation=='U')) && (cardToken.isEmpty))
    {
      showalertDailog(context,'Error','Card Token should not be empty');
      ResponseConfig.startTrxn = false;
    }
    else if ((Action=='12') &&(cardOperation=='D') && cardToken.isEmpty)
    {
      showalertDailog(context,'Error','Card Token should not be empty');
      ResponseConfig.startTrxn = false;
    }
   /* else if ((Action=="12")&&((cardOperation !='A') || (cardOperation!='U')) ||(cardOperation !='D'))
    {
//      alert.showAlertDialog(context, "Invalid Tokenization", "Card Operation is not proper1 ", false);
    print('In else  $Action : $cardOperation');
      showalertDailog(context,'Invalid Tokenization','Card Operation is not proper ');
      ResponseConfig.startTrxn = false;
    }*/
    else if ((Action=="14")&&(cardToken.isEmpty))
    {
//      alert.showAlertDialog(context, "Invalid Refund", "Card Token Should not be empty ", false);
      showalertDailog(context, 'Invalid Refund', 'Card Token should not be empty ');
      ResponseConfig.startTrxn = false;
    }
    else
    {
      d=true;
    }
    return d;
  }

  static void showalertDailog(BuildContext context,String title,String description)
  {

      AlertDialog alert =  AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15)),
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  /*Positioned(
                    left: Constantvals.padding,
                    right: Constantvals.padding,
                    child: CircleAvatar(
                      backgroundColor: Colors.blueAccent,
                      radius: Constantvals.avatarRadius,
                    ),
                  ),*/
                  Container(

                    margin: EdgeInsets.only(top: Constantvals.marginTop),

                    child: Column(
                      mainAxisSize: MainAxisSize.min, // To make the card compact
                      children: <Widget>[
                      /*  CircleAvatar(
                          radius: 55,
                          backgroundColor: Color(0xffFDCF09),
                          child: CircleAvatar(
                            radius: 50,
                            backgroundImage: AssetImage('images/batman.jpg'),
                          ),
                        ),*/
                        Text(
                          title,
                          style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 16.0),
                        Text(
                          description,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 24.0),
                        Align(
                          alignment: Alignment.center,

                          child: FlatButton(
                            color: Colors.deepOrange,
                            onPressed: () {
                              Navigator.of(context).pop();// To close the dialog//todo close plugin
                              ResponseConfig.startTrxn=false;
                            },
                            child: Text('OK'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              elevation: 10,
            );
      showDialog(
        context: context,

        barrierDismissible: false,
        builder: (BuildContext context) {
          return alert;
        },
      );


  }
  static bool isValidEmailchk(String emailCheck) {
//    Pattern pattern = r"[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@([a-z]+\\.[a-z]+\\.[a-z])";
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@[a-z]+\.+[a-z]$';
    RegExp regExp = new RegExp(pattern);
    bool chk = regExp.hasMatch(emailCheck);
    print('$chk');
    return chk;
  }
  }

////    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@([a-z]+\\.[a-z]+(\\.[a-z]+)").hasMatch(emailCheck);
////    print('$emailValid');
////    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';
//
////    RegExp regExp = new RegExp(p);
//    RegExp regExp = new RegExp(pattern);
//    print('$emailCheck');
//    bool chk=regExp.hasMatch(emailCheck);
//print('email $chk');
//    return chk;
//  }
//  static String validateEmail(String value) {
//    Pattern pattern =
//        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//    RegExp regex = new RegExp(pattern);
//    if (!regex.hasMatch(value))
//      return 'Enter Valid Email';
//    else
//      return 'Runali Data';
//  }
 }
