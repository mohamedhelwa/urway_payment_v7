

class TrxnRespModel
{  final String TranId;
  final String ResponseCode;
  final String amount;
  final String result;
  final String payId;
  final String cardToken;


TrxnRespModel({this.TranId, this.ResponseCode, this.amount, this.result,
      this.payId, this.cardToken});

  factory TrxnRespModel.fromJson(Map<String, dynamic> json) {
  return TrxnRespModel(
    TranId: json['TranId'],
    ResponseCode: json['ResponseCode'],
    amount: json['amount'],
    result: json['result'],
    payId: json['payId'],
    cardToken: json['cardToken'],

  );
  }

  Map toMap() {
  var map = new Map<String, dynamic>();
  map["TranId"] = TranId;
  map["ResponseCode"] =ResponseCode;
  map["amount"] =amount;
  map["result"] =result;
  map["payId"] =payId;
  map["cardToken"] =cardToken;
  return map;
  }
}
