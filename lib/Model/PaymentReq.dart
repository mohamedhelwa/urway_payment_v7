class PaymentReq
{
  String terminalId;
  String password;
  String action;
  String currency;
  String customerEmail;
  String country;
  String amount;
  String customerIp;

  String merchantIp;
  String trackid;
  String udf1;
  String udf2;
  String udf3;
  String udf4;
  String udf5;
  String udf7;
  String address;
  String city;
  String zipCode;
  String state;
  String tokenOperation;
  String cardToken;
  String tokenizationType;
  String requestHash;


  PaymentReq({this.terminalId, this.password, this.action, this.currency,
      this.customerEmail, this.country, this.amount, this.customerIp,
      this.merchantIp, this.trackid, this.udf1, this.udf2, this.udf3, this.udf4,
      this.udf5, this.udf7, this.address, this.city, this.zipCode, this.state,this.tokenOperation,
      this.cardToken, this.tokenizationType,this.requestHash});


  //action12
  PaymentReq.tokenize({this.terminalId, this.password, this.action, this.currency,
    this.customerEmail, this.country, this.amount, this.customerIp,
    this.merchantIp, this.trackid, this.udf1, this.udf2, this.udf3, this.udf4,
    this.udf5, this.udf7,this.tokenOperation,
    this.cardToken, this.requestHash});

//  action 14
  PaymentReq.refund({this.terminalId, this.password, this.action, this.currency,
    this.customerEmail, this.country, this.amount, this.customerIp,
    this.merchantIp, this.trackid, this.udf1, this.udf2, this.udf3, this.udf4,
    this.udf5, this.udf7,
    this.cardToken,this.requestHash});

  factory PaymentReq.fromJson(Map<String, dynamic> json)
  {
    PaymentReq pay;
     pay=PaymentReq(
        terminalId:json['terminalId'] as String,
        password:json['password'] as String,
        action:json['action'] as String,
        currency:json['currency'] as String,

        customerEmail:json['customerEmail'] as String,
        country:json['country'] as String,
        amount:json['amount'] as String,
        customerIp:json['customerIp'] as String,

        merchantIp:json['merchantIp'] as String,
        trackid:json['trackid'] as String,
        udf1:json['udf1'] as String,
        udf2:json['udf2'] as String,
        udf3:json['udf3'] as String,
        udf4:json['udf4'] as String,
        udf5:json['udf5'] as String,
        udf7:json['udf7'] as String,
        address:json['address'] as String,
        city:json['city'] as String,
        zipCode:json['zipCode'] as String,
        state:json['state'] as String,
        cardToken:json['cardToken'] as String,
        tokenOperation:json['tokenOperation'] as String,
        tokenizationType:json['tokenizationType'] as String,
        requestHash: json['requestHash'] as String
    );
     pay=PaymentReq.tokenize(terminalId:json['terminalId'] as String,
         password:json['password'] as String,
         action:json['action'] as String,
         currency:json['currency'] as String,

         customerEmail:json['customerEmail'] as String,
         country:json['country'] as String,
         amount:json['amount'] as String,
         customerIp:json['customerIp'] as String,

         merchantIp:json['merchantIp'] as String,
         trackid:json['trackid'] as String,
         udf1:json['udf1'] as String,
         udf2:json['udf2'] as String,
         udf3:json['udf3'] as String,
         udf4:json['udf4'] as String,
         udf5:json['udf5'] as String,
         udf7:json['udf7'] as String,

         cardToken:json['cardToken'] as String,
         tokenOperation:json['tokenOperation'] as String,
         requestHash: json['requestHash'] as String
     );


     pay=PaymentReq.refund(
         terminalId:json['terminalId'] as String,
         password:json['password'] as String,
         action:json['action'] as String,
         currency:json['currency'] as String,

         customerEmail:json['customerEmail'] as String,
         country:json['country'] as String,
         amount:json['amount'] as String,
         customerIp:json['customerIp'] as String,

         merchantIp:json['merchantIp'] as String,
         trackid:json['trackid'] as String,
         udf1:json['udf1'] as String,
         udf2:json['udf2'] as String,
         udf3:json['udf3'] as String,
         udf4:json['udf4'] as String,
         udf5:json['udf5'] as String,
         udf7:json['udf7'] as String,

         cardToken:json['cardToken'] as String,
         requestHash: json['requestHash'] as String
     );
     return pay;
  }


  Map toMap() {
    var map = new Map<String, dynamic>();
    map["terminalId"] = terminalId;
    map["password"] = password;
    map["action"]= action;
    map["currency"]=currency;
    map["customerEmail"]=customerEmail;
    map["country"] =country;
    map["amount"] =amount;
    map["customerIp"]=customerIp ;
    map["merchantIp"] =merchantIp;;
    map["trackid"] =trackid;
    map["udf1"] =udf1;
    map["udf2"]=udf2;
    map["udf3"]=udf3;
    map["udf4"] =udf4;
    map["udf5"]=udf5;
    map["udf7"]=udf7;
    map["address"]=address;
    map["city"]=city;
    map["zipCode"]=zipCode;
    map["state"]=state;
    map["cardToken"]=cardToken;
    map["tokenOperation"]=tokenOperation;
    map["tokenizationType"]=tokenizationType;
    map["requestHash"]=requestHash;


    return map;
  }


}
